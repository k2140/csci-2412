<?php
$numSlides = 3;

$jems = Jem::getJemsFromDB($conn, $numSlides);
$numSlides = count($jems) < $numSlides ? count($jems) : $numSlides;

if ($numSlides > 0) {
?>
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <?php
    for ($i = 1; $i < $numSlides; $i++) { ?>
      <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="<?php echo $i; ?>" aria-label="Slide <?php echo $i + 1; ?>"></button>
    <?php
    }
    ?>
  </div>
  <div class="carousel-inner">
    <?php
    foreach ($jems as $index => $jem) {
    ?>
      <div class="carousel-item <?php echo ($index == 0 ? 'active"' : '"'); ?> data-bs-interval=" 10000">
        <svg class="bd-placeholder-img bd-placeholder-img-lg d-block w-100" width="800" height="400" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777"></rect>
        </svg>
        <div class="carousel-caption d-xs-block">
          <h3><?php echo $jem->overview; ?></h3>
          <p><?php echo $jem->publishDate; ?></p>
        </div>
      </div>
    <?php
    }
    ?>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
  </div>
<?php
}
?>