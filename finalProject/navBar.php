<?php
include("jem.php");
include("utils.php");
$conn = connect_to_db("finalProjectKrisKettendorf");
session_start();
?>

<html>

<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
</head>

<header>
  <div class="px-3 py-2 bg-dark text-white">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start padding-right 25px">
        <a href="/" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none">
          <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap">
            <use xlink:href="#bootstrap"></use>
          </svg>
        </a>
        <h1 style="padding-top: 10px; font-size: 75px;">Jemery</h1>
        <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
          <li>
            <a href="homepage.php" class="nav-link text-secondary">
              <svg class="bi d-block mx-auto mb-1" width="24" height="24">
                <use xlink:href="#home"></use>
              </svg>
              Home
            </a>
          </li>
          <!-- <li>
            <a href="aboutMe.php" class="nav-link text-white">
              <svg class="bi d-block mx-auto mb-1" width="24" height="24">
                <use xlink:href="#speedometer2"></use>
              </svg>
              About
            </a>
          </li> -->
          <?php if (isset($_SESSION['username'])) { ?>
            <li>
              <a href="splash.php" class="nav-link text-white">
                <svg class="bi d-block mx-auto mb-1" width="24" height="24">
                  <use xlink:href="#speedometer2"></use>
                </svg>
                My Profile
              </a>
            </li>
          <?php } else "" ?>
        </ul>
      </div>
    </div>
  </div>
  <div class="px-3 py-2 border-bottom mb-3">

    <div class="text-end">
      <?php if (isset($_SESSION['username'])) { ?>
        <button type='button' class='btn btn-light text-dark me-2'><a href='logoutPage.php' style='text-decoration:none'>Logout</a></button>
      <?php } else { ?>
        <button type='button' class='btn btn-light text-dark me-2'><a href='loginPage.php' style='text-decoration:none'>Login</a></button>
        <button type='button' class='btn btn-primary'><a href='signUpPage.php' style='text-decoration:none; color:white;'>Sign-up</a></button>
      <?php } ?>
    </div>
  </div>
  </div>

</html>