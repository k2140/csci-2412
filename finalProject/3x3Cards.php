<?php
?>

<html>

<head>
  <script src="https://kit.fontawesome.com/989ffb2b9a.js" crossorigin="anonymous"></script>
</head>
<div class="container" style="text-decoration:none">
  <div class="row">
    <div class="d-grid gap-3" style="grid-template-columns: 1fr 2fr 1fr;">
      <?php
      $data = Jem::matchJemToUser($conn, $_SESSION['userId']);
      foreach ($data as $jem) {
      ?>
        <div class="bg-light border rounded-3" style="text-align:center;">
          <div class="card-wrapper">
            <div class="card">
              <br>
              <a href="shareJem.php" style="text-decoration:none; font-size: 30px; margin-left: 200px; margin-top:-10px"></nbsp><i class="fas fa-share-alt"></i></a>
              Date Created:<p><?php echo $jem->publishDate ?></p>
              <div><?php if (!empty($jem->primaryImage)) { ?>
                  <img style="max-width:90%; height:auto; justify-content:center" src='data:image/jpeg;base64,<?php echo base64_encode($jem->primaryImage) ?>' alt=<?php $primaryImageAltText ?> />
                  <!-- https://www.w3schools.com/howto/howto_css_tooltip.asp -->
                <?php } ?>
              </div>
              Overview:<p href="myJems.php?jemId=<?php echo $jem->jemId ?>"><?php echo $jem->overview ?></p>
              Whereabouts: <p><?php echo $jem->whereabouts ?></p>
              Context And Dialogue: <textarea rows="5" cols="40"><?php echo $jem->contextAndDialogue ?></textarea>
            </div>
          </div>
          <a class='btn btn-danger' style="background-color:blue; color:white" href='./editJem.php?jemId=<?php echo $jem->jemId ?>'>Edit</a>
          <a class='btn btn-danger' href='./deleteJem.php?deleteJemId=<?php echo $jem->jemId ?>'>Delete</a>
          <br><br><br><br><br><br><br><br><br><br>
        </div>
      <?php
      }
      ?>
    </div>
  </div>
</div>

</html>