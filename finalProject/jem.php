
<?php
class Jem
{
  // parameters
  public $conn, $jemId, $overview, $whereabouts, $contextAndDialogue, $primaryImage, $primaryImageAltText;
  // constructing properties
  function __construct($conn, $jemInfo)
  {
    $this->conn = $conn;
    $this->jemId = $jemInfo['jemId'];
    $this->overview = $jemInfo['overview'];
    $this->whereabouts = $jemInfo['whereabouts'];
    $this->contextAndDialogue = $jemInfo['contextAndDialogue'];
    $this->primaryImage = $jemInfo['primaryImage'];
    $this->primaryImageAltText = $jemInfo['primaryImageAltText'];
    $this->publishDate = $jemInfo['publishDate'];
  }
  // destruct
  function __destruct()
  {
  }
  // static fx to grab and return Jems from DB
  static function getJemsFromDB($conn, $numJems = 3)
  {
    $selectedJems = "SELECT jems.*, users.userName
    FROM jems
    LEFT JOIN users ON users.UserName
    LIMIT :numJems";
    $stmt = $conn->prepare($selectedJems);
    $stmt->bindParam(':numJems', $numJems, PDO::PARAM_INT);
    $stmt->execute();

    $jemList = array();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt->fetchAll() as $listRow) {
      $jem = new Jem($conn, $listRow);
      $jemList[] = $jem;
    }
    return $jemList;
  }
  // static fx to grab and return single Jem from DB (so we can route to new page by Id and fill form with existing data)
  static function getJemById($conn, $jemId)
  {
    $selectJemById = "SELECT * 
    FROM jems
    WHERE jemId=:jemId";
    $stmt = $conn->prepare($selectJemById);
    $stmt->bindParam(":jemId", $jemId, PDO::PARAM_INT);
    $stmt->execute();

    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt->fetchAll() as $listRow) {
      $jem = new Jem($conn, $listRow);
    }
    return $jem;
  }
  // all jems associated with particular user
  // make sure we're pulling the right info from the DB
  static function matchJemToUser($conn, $userId)
  {
    // match jemId to userId to post the corresponding jem
    $selectJemByUser = "SELECT *
    FROM jems
    WHERE jemUserId=:userId";
    $stmt = $conn->prepare($selectJemByUser);
    $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);
    $stmt->execute();

    $jemList = array();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt->fetchAll() as $listRow) {
      $jem = new Jem($conn, $listRow);
      $jemList[] = $jem;
    }
    return $jemList;
  }

  function createJem($userId)
  {
    print_r($userId);
    $insert = "INSERT INTO jems
        (overview, whereabouts, contextAndDialogue, publishDate, primaryImage, primaryImageAltText, jemUserId)
        VALUES
        (:overview, :whereabouts, :contextAndDialogue, :publishDate, :primaryImage, :primaryImageAltText, :jemUserId)";
    $stmt = $this->conn->prepare($insert);
    $stmt->bindParam(':overview', $this->overview);
    $stmt->bindParam(':whereabouts', $this->whereabouts);
    $stmt->bindParam(':contextAndDialogue', $this->contextAndDialogue);
    $stmt->bindParam(':publishDate', $this->publishDate);
    $stmt->bindParam(':primaryImage', $this->primaryImage);
    $stmt->bindParam(':primaryImageAltText', $this->primaryImageAltText);
    $stmt->bindParam(':jemUserId', $userId);
    $stmt->execute();
  }

  function editJem()
  {
    $update = "UPDATE jems 
    SET overview=:overview, whereabouts=:whereabouts, contextAndDialogue=:contextAndDialogue, primaryImage=:primaryImage, primaryImageAltText=:primaryImageAltText, publishDate=:publishDate
    WHERE jemid=:jemId";
    $stmt = $this->conn->prepare($update);
    $stmt->bindParam(':jemId', $this->jemId, PDO::PARAM_INT);
    $stmt->bindParam(':overview', $this->overview);
    $stmt->bindParam(':whereabouts', $this->whereabouts);
    $stmt->bindParam(':contextAndDialogue', $this->contextAndDialogue);
    $stmt->bindParam(':primaryImage', $this->primaryImage);
    $stmt->bindParam(':primaryImageAltText', $this->primaryImageAltText);
    $stmt->bindParam(':publishDate', $this->publishDate);
    $stmt->execute();
  }

  function deleteJem()
  {
    $delete = "DELETE FROM jems WHERE jemId=:jemId";
    $stmt = $this->conn->prepare($delete);
    $stmt->bindParam(':jemId', $this->jemId, PDO::PARAM_INT);
    $stmt->execute();
  }
}

?>

