<?php
include("../navBar.php");

// gets href from form
$jemId = $_GET['deleteJemId'];

if (isset($jemId)) {
  try {
    $jem = Jem::getJemById($conn, $jemId);
  } catch (Exception) {
    header("Location: splash.php");
  }
} else {
  header("Location: splash.php");
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $jem->deleteJem();
  header("Location: splash.php");
}
?>

<div class="container">
  <div class="row justify-content-center text-center">
    <div class="col-md-10 col-lg-8">
      <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <label for="submit">Are you sure you want to delete this jem?</label><br><br>
        <input type="submit" class="btn btn-danger" value="Delete">
      </form>
    </div>
    <div class="col-md-10 col-lg-8">
      <a href="splash.php" class="btn btn-success">Cancel</a>
    </div>
  </div>
</div>