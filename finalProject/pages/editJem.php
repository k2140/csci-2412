<?php
include("../navBar.php");

if (isset($_GET['jemId'])) {
  try {
    $jem = Jem::getJemById($conn, $_GET['jemId']);
  } catch (Exception) {
    header("Location: splash.php");
  }
} else {
  header("Location: splash.php");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $overview = clean_input($_POST['overview']);
  $whereabouts = clean_input($_POST['whereabouts']);
  $contextAndDialogue = clean_input($_POST['contextAndDialogue']);
  $publishDate = clean_input($_POST['publishDate']);

  if (!empty($overview) && !empty($whereabouts) && !empty($contextAndDialogue) && !empty($publishDate)) {
    $jem->overview = $overview;
    $jem->whereabouts = $whereabouts;
    $jem->contextAndDialogue = $contextAndDialogue;
    $jem->publishDate = $publishDate;
    $jem->editJem();
    header('Location: splash.php');
  }
}
?>

<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border:1px solid #ccc; text-align:center" enctype="multipart/form-data">
  <form>
    <p><b style="font-size:25px;"><u>Edit Jem</u></b></p>
    <label for="overview">Overview:</label><br>
    <input type="text" id="overview" name="overview" value="<?php echo $jem->overview ?>"><br>
    <label for="whereabouts">Whereabouts:</label><br>
    <input type="text" id="whereabouts" name="whereabouts" value="<?php echo $jem->whereabouts ?>"><br>
    <label for="contextAndDialogue">Context and Dialouge:</label><br>
    <textarea type="text" id="contextAndDialogue" name="contextAndDialogue" rows="5" cols="100"><?php echo $jem->contextAndDialogue ?></textarea><br>
    <label for="publishDate">Publish Date:</label><br>
    <input type="date" class="publishDate" id="publishDate" name="publishDate" value="<?php echo $jem->publishDate ?>"><br>
    <div><?php if (!empty($jem->primaryImage)) { ?>
        <img style="max-width:90%; height:auto; justify-content:center" src='data:image/jpeg;base64,<?php echo base64_encode($jem->primaryImage) ?>' alt=<?php $primaryImageAltText ?> />
        <!-- https://www.w3schools.com/howto/howto_css_tooltip.asp -->
      <?php } ?>
    </div>
    <div class="form-group">
      <label for="secondaryImage">Select another image to upload:</label>
      <input type="file" name="secondaryImage" id="secondaryImage">
    </div>
    <label for="primaryImageAltText">Image Description:</label><br>
    <input type="text" id="primaryImageAltText" name="primaryImageAltText" value="<?php echo $jem->primaryImageAltText ?>"><br>
    <!-- want to be able to remove existing picture -->
    <br><input type="submit" value="Submit"><br>
    <button type="text" href="Location: splash.php">Back</button>
  </form>
</form>

<?php
include("../footer.php");
?>