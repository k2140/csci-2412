<?php
include("../navBar.php");

if (isset($_SESSION['username'])) {
?>
  <div class="container-wrap">
    <div class='container-splash'>
      <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
          <h1 style="text-align:center; margin: 20px;">Hello <?php echo $_SESSION['username'] ?>!</h1>
        </div>
      </div>
    </div>
    </header>
    <div class="container-fluid pb-3">
      <div class="d-grid gap-3" style="grid-template-columns: 1fr; margin:0px, 50px;">
        <div class="bg-light border rounded-3" style="text-align:center">
          <br><br><br><br><br><span><a href="createJem.php">Create a Jem</a></span><br><br><br><br><br>
        </div>
      </div>
      <h2 style="text-align:center">My Jems:</h2>
    </div>
  </div>
  <?php
  include("../3x3Cards.php");
  include("../footer.php");
  ?>

  </html>

<?php
} else {
?>

  <a href="login.php">Please log in to see this page</a>

<?php
}
?>