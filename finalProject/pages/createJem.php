<?php
include('../navBar.php');
include_once('../utils.php');
$imageErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $mbFileSize = $_FILES['primaryImage']['size'] / 1000000;
  if ($mbFileSize > 10) {
    $imageErr = "Your file is too large. Max file size is 10MB. Yours was $mbFileSize";
  }
  $overview = clean_input($_POST['overview']);
  $whereabouts = clean_input($_POST['whereabouts']);
  $contextAndDialogue = clean_input($_POST['contextAndDialogue']);
  $primaryImage = file_get_contents($_FILES['primaryImage']['tmp_name']);
  $primaryImageAltText = clean_input($_POST['primaryImageAltText']);

  $jemUserId = $_SESSION['userId'];
  $publishDate = false;
  if (isset($_POST['publishDate'])) {
    $publishDate = true;
  }

  if (!empty($overview) && !empty($whereabouts) && !empty($contextAndDialogue && !empty($primaryImage) && !empty($primaryImageAltText))) {

    $publishDate = date('y-m-d');

    $jemInfo = array(
      'overview' => $overview,
      'whereabouts' => $whereabouts,
      'contextAndDialogue' => $contextAndDialogue,
      'publishDate' => $publishDate,
      'primaryImage' => $primaryImage,
      'primaryImageAltText' => $primaryImageAltText,
      'jemUserId' =>  $jemUserId
    );
    $jem = new Jem($conn, $jemInfo);
    $jem->createJem($_SESSION['userId']);
    header("Location: splash.php");
  }
}
?>
<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border:1px solid #ccc; text-align:center" enctype="multipart/form-data">
  <form>
    <p><b style="font-size:25px;"><u>Create a Jem</u></b></p>
    <label for="overview" style="text-decoration:underine">Overview:</label><br>
    <input type="text" id="overview" name="overview" required><br>
    <label for="whereabouts" style="text-decoration:underine">Whereabouts:</label><br>
    <input type="text" id="whereabouts" name="whereabouts" required><br>
    <label for="contextAndDialogue" style="text-decoration:underine">Context and Dialouge:</label><br>
    <input type="text" id="contextAndDialogue" name="contextAndDialogue" required><br>
    <label for="publishDate" style="text-decoration:underine">Publish Date:</label><br>
    <input type="date" class="publishDate" id="publishDate" required><br>
    <div class="form-group">
      <label for="primaryImage" style="text-decoration:underine">Select image to upload:</label><br>
      <input type="file" name="primaryImage" id="primaryImage" required>
      <span class="error">* <?php echo $imageErr; ?></span><br>
    </div>
    <label class="primaryImageAltText" style="text-decoration:underine">Image Description:</label>
    <input type="text" name="primaryImageAltText" required></input><br>
    <input type="submit" value="Submit">
  </form>
</form>
<?php
include("../footer.php")
?>
<!-- var_dump() returns all the data-->