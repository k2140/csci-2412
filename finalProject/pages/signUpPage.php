<?php
include("../navBar.php");
$name = $username = $password = "";
$userNameErr = $passwordErr = "";

// when adding data to DB: make sure "" in $_POST === name attribute in form input field && 'PHP SELF' statement is under action attribute
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = clean_input($_POST["fullName"]);
  $username = clean_input($_POST["email"]);
  $password1 = clean_input($_POST["password1"]);
  $password2 = clean_input($_POST["password2"]);

  $userNameErr = checkUsernameIsUnique($username);

  if ($password1 !== $password2) {
    $password = "";
    $passwordErr = "(Passwords do not match.)";
  } else {
    $password = $password1 = password_hash($password1, PASSWORD_DEFAULT);
  }

  if ($userNameErr === "" && $passwordErr === "") {
    addUser($name, $username, $password);
    $_SESSION['username'] = $username;
    header("Location: splash.php");
  }
}

function checkUsernameIsUnique($username)
{
  $conn = connect_to_db("finalProjectKrisKettendorf");
  $selectUser = "SELECT userName FROM users WHERE userName=:userName";
  $stmt = $conn->prepare($selectUser);
  $stmt->bindParam(':userName', $username);
  $stmt->execute();

  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  return empty($stmt->fetchAll()) ? "" : "(Username is already taken)";
}

function addUser($name, $username, $password)
{
  $conn = connect_to_db("finalProjectKrisKettendorf");
  $insert = "INSERT INTO users (fullName, userName, userPassword)
    VALUES (:name, :userName, :password)";
  $stmt = $conn->prepare($insert);
  $stmt->bindParam(':name', $name);
  $stmt->bindParam(':userName', $username);
  $stmt->bindParam(':password', $password);
  $stmt->execute();
}
?>

<!-- post completes form submission -->
<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>" style="border:1px solid #ccc; text-align:center">
  <div class="container">
    <h1>Sign Up</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>
    <span><?php echo $userNameErr ?></span><br>
    <label for="fullName"><b>Full Name</b></label>
    <input type="text" placeholder="Full Name" name="fullName" alt="Enter your full name here"> <br>
    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" alt="Enter your email here" required><br>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password1" alt="Enter a password here" required><br>
    <label for="psw-repeat"><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" name="password2" alt="Type your password again to confirm" required><br>
    <span><?php echo $passwordErr ?></span><br>

    <label>
      <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px" alt="Check this box to save your credentials for this site"> Remember me
    </label>

    <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

    <div class="clearfix">
      <button type="button" class="cancelbtn" alt="Click here to cancel signup"><a href="homepage.php">Cancel</a></button>
      <button type="submit" class="signupbtn" alt="Click here to complete signup process">Sign Up</button>
    </div>
  </div>
</form>

<?php
include("../footer.php");
?>