<!doctype html>
<html lang="en">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--fit to scale -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.rtl.min.css" integrity="sha384-+qdLaIRZfNu4cVPK/PxJJEy0B0f3Ugv8i482AKY7gwXwhaCroABd086ybrVKTa0q" crossorigin="anonymous">
<?php
include("../navBar.php");
?>

<div class="container" style="background-image: linear-gradient(to top, rgba(255,0,0,0), rgba(173, 216, 230)); font-style:oblique; border-radius:50%; margin-top: 10px">
  <h1 style="font-size: 30px; text-align:center; margin-top: 20px"><br>Cue cards to capture your experiences.</h1>
  <img src="../images/notepad.jpg" class="img-thumbnail" style="margin-top: 15px; border-radius: 65%;" alt="this is an upclose image of a notepad with a pencil resting on the edge of it">
</div>
