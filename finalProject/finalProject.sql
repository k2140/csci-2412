DROP DATABASE IF EXISTS finalProjectKrisKettendorf;
CREATE DATABASE finalProjectKrisKettendorf;

USE finalProjectKrisKettendorf;

DROP TABLE IF EXISTS users;
CREATE TABLE users (
  userId INT PRIMARY KEY AUTO_INCREMENT,
  fullName VARCHAR(100),
  userName VARCHAR(50),
  userPassword VARCHAR(300)
);

INSERT INTO users (fullName, userName, userPassword)
VALUES ("Kris Kettendorf", "jbeclectic", "Iamtheadmin"), ("Bobby Flay", "bobbO43", "123easypeezy");

SELECT * FROM users;

DROP TABLE IF EXISTS jems;
CREATE TABLE jems (
  jemId INT PRIMARY KEY AUTO_INCREMENT,
  overview VARCHAR(100),
  whereabouts VARCHAR(100),
  contextAndDialogue VARCHAR(1000),
  publishDate DATE,
  primaryImage MEDIUMBLOB,
  primaryImageAltText VARCHAR(255),
  -- secondaryImage...etc altText
  jemUserId INT,
  FOREIGN KEY (jemUserId) REFERENCES users(userId) ON DELETE CASCADE
);

INSERT INTO jems (overview, whereabouts, contextAndDialogue, publishDate, jemUserId)
VALUES (
"**SAMPLE** This section will contain a brief overview", 
"Document the location of the overview here.", 
"Write out all the details you remember that capture the overview (pointers, key information about a person or event that come up in conversation for future reference)",
"2022-04-06", 
2
);

SELECT * FROM jems;

DROP TABLE IF EXISTS contactInfo;
CREATE TABLE contactInfo (
  contactInfoId INT PRIMARY KEY AUTO_INCREMENT,
  userName VARCHAR(40),
  fullName VARCHAR(100),
  emailAddress VARCHAR(40),
  streetAddress VARCHAR(40),
  areaOfInterest VARCHAR(20),
  FOREIGN KEY (contactInfoId) REFERENCES users(userId) ON DELETE CASCADE
);

INSERT INTO contactInfo (userName, fullName, emailAddress, streetAddress, areaOfInterest)
VALUES 
("jbeclectic", "Kris Kettendorf", "kkettendorf@hotmail.com", "Somewhere In Delaware", "Columbus, Ohio"),
("rclower", "Robin Clower", "rclower@cscc.edu", "Delaware Hall 202", "Columbus Ohio");

SELECT * FROM contactInfo;

-- Jem = doucmentation of an interaction/experience...little "contact cue cards" ref PolarSteps
-- a Jem contains: people, places, any context/dialogue (pointers, important things that come up in conversation/highlights of event for future reference)
-- can tables contain multiple foreign keys?