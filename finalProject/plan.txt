Todo:


be able to remove photo in editing
be able to add a second photo?
add "key words and phrases" section to form
underline labels for form fields

about page on homepage should be about developer of site (me)
about page once logged in should render information about the user (user greeting)
existing users should be able to update their personal information and it reflect in the Database (add 'userInfomation' form to about page of user)
  aboutDeveloper.php
  aboutUser.php
  ternary logic inside of navBar

Complete:
(x) photo displays when editing
(x) font-awesome web kit not displaying share icon for button
(x) make sure it deletes the correct jem
(x) when logged in, 'my profile' button should take you to splash.php
(x) new jems are being created in the db, but not being assigned to the user
(x) include edit buttons once jem is created
(x) new jems should reflect in DB after submission and populate under 'My Jems' section
<!-- (x) when you're on the log-in page the log-in and sign-up buttons should disappear -->
<!-- (x) Create Jem form fields: title location date summary (be able to keep a ongoing 'log'/dates of interaction with someone, chronology of interaction) -->
<!-- (x) route user to splash page after logging in -->
<!-- (x) Upon successful sign up, a user should get logged in by default and see their new splash page -->
<!-- (x) verifyUsernameIsUnique fx upon sign-up not working -->
<!-- (x) error checking for incorrect password/ password's do not match upon sign up -->
<!-- (x) succesfully add user in DB upon sign in -->
<!-- (x) click of log-in button and reroutes to homepage after complete -->
<!-- (x)Get SQL scripts running, be able to access data through cms -->
<!-- (x) display jems data from DB under My Jems -->
<!-- (x) Link "login" and "sign up" buttons to appropriate pages https://stackoverflow.com/questions/59805709/how-to-open-the-modal-popup-using-a-link-instead-of-button-->
<!-- (x) Route Create/Update Jems accordingly -->
