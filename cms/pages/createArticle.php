<?php
include("../includes/navbar.php");

// get the values from our POSTed form inside of createArticle.php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $title = clean_input($_POST["title"]);
  // print_r($_POST);
  $content = clean_input($_POST["content"]);

  $isPublished = false;
  if (isset($_POST['publish'])) {
    $isPublished = true;
  }

  if (!empty($title) && !empty($content)) {
    $authorId = getUserId($conn, $_SESSION['username']);
    $publishDate = date('Y-m-d');

    $articleInfo = array(
      "articleId" => "",
      "publishDate" => $publishDate,
      "isPublished" => $isPublished,
      "title" => $title,
      "content" => $content,
      "author" => $authorId
    );

    $article = new Article($conn, $articleInfo);
    $article->createArticle();
    header("Location: ArticleListing.php");
  }
}

?>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10 col-lg-8 col-xl-7">
      <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div class="form-group">
          <label for="title">Title</label>
          <span class="error">*<br>
            <input type="text" class="form-control" name="title" id="title" required>
        </div>
        <div class="form-group">
          <label for="content">Content</label>
          <span class="error">*<br>
            <textarea rows="10" class="form-control" name="content" id="content" required></textarea>
        </div>
        <div class="form-group">
          <label for="publish">Publish</label>
          <input type="checkbox" id="publish" name="publish">
        </div>
        <input type="submit" class="btn btn-primary" value="Submit">
      </form>
    </div>
  </div>
</div>