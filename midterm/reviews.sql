CREATE DATABASE midtermKris;
USE midtermKris;

CREATE TABLE reviews (
  reviewId INT PRIMARY KEY AUTO_INCREMENT,
  reviewText VARCHAR(255),
  numStars INT 
);

INSERT into reviews (reviewText, numStars)
VALUES 
  ("This database is a nice start...", 1),
  ("Wow. Databases are incredible!", 5);

SELECT * FROM reviews WHERE numStars < 3;
