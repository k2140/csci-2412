<?php
$servername = "localhost";
$username = "root";
$password = "";

try {
  // create PDO object
  $conn = new PDO("mysql:host=$servername;dbname=lab6", $username, $password);
  echo "Connected successfully";
} catch (PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}

// create SQL query & printing out data into our browser that is coming from our database.
$olympians = "SELECT * FROM Olympians";
$stmt = $conn->prepare($olympians);
$stmt->execute();
$stmt->setFetchMode(PDO::FETCH_ASSOC);
// print_r($stmt->fetchAll());
foreach ($stmt->fetchAll() as $allOlympians) {
  echo "<br>";
  foreach ($allOlympians as $olympian) {
    echo "$olympian <br>";
  }
}

// https://docs.google.com/document/d/1ca8pvIhySme4GnJfoWNnSupY4ScJIV6ynnJUy9kCyfM/edit#
