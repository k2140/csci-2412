-- CREATE DATABASE toDoList;
USE toDoList;
 
DROP TABLE IF EXISTS items;
CREATE TABLE items (
  itemId INT PRIMARY KEY IDENTITY,
  toDoItem VARCHAR(255), 
  isComplete BOOL
);
 
INSERT INTO items (toDoItem, isComplete)
VALUES
("Finish lab 7", false),
("Learn how to update records", false),
("Graduate first grade", true);

SELECT * FROM items;


-- Create DB: 1. Connect to DB Run DB 2. Clean input
-- 