CREATE TABLE movies(
    primaryKey INT IDENTITY PRIMARY KEY, 
    title VARCHAR(255),
    director VARCHAR(255),
    releaseDate DATE
    );

INSERT INTO movies(title, director, releaseDate)
VALUES 
    ("Man on Fire", "Tony Scott", "2004-04-21"), 
    ("Rainman", "Barry Levinson", "1988-12-12");

CREATE TABLE pokerTerms(
    primaryKey INT IDENTITY PRIMARY KEY, 
    startingHands VARCHAR(255),
    boardTexture VARCHAR(255),
    numOfPlayers NUMBER,
    opponentPlayerType  VARCHAR(255),
    profitableSession BOOLEAN,
    datePlayed DATE
);

INSERT INTO pokerTerms(startingHands, boardTexture, numOfPlayers, opponentPlayerType, profitableSession, datePlayed)
VALUES
    ("Pocket Kings (KK)", "Q82", 3, "NIT, LAG", true, "2022, 02, 20");

DROP TABLE IF EXISTS pokerTerms;