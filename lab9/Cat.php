<?php
include("Animal.php");

class Cat extends Animal
{
  public function amountOfTimeSpentSleeping()
  {
    $amountOfSleepPerDay = $this->age * 365 * 15;
    echo "$this->name has spent $amountOfSleepPerDay hours sleeping.";
  }
}

$milo = new Cat('Milo', 4);
$milo->amountOfTimeSpentSleeping();
